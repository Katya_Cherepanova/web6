function poleKol(){
    let k = document.getElementById("kol").value;
    if(k.match(/^[0-9]+$/) === null){
        document.getElementById("price").innerHTML = "Неверные данные";
        return false;
    }else{
        return true;
    }
}

function polePrice() {
    return {
        Nikon: [22000, 68000, 446000],
        vibor: {
            opt1 : 0,
            opt2: 10000,
            opt3: 35000,
            opt4: 24000,
        },
        doppole: {
            dop1: 1500,
            dop2: 500,
            dop3: 4000,
        },
    };
}
function price(){
    let vid = document.getElementById("Nikon");
    let kol = document.getElementById("kol").value;
    let radk = document.getElementById("radioknop");
    let box = document.getElementById("checkboxepole");
    let price = document.getElementById("price");
    price.innerHTML = "0";
    switch (vid.value) {
        case "1":
            radk.style.display = "none";
            box.style.display = "none";
            if(poleKol()){
                let itog = parseInt(kol) * polePrice().Nikon[0];
                price.innerHTML = itog + " рублей";
            }
            break;
        case "2":
            radk.style.display = "block";
            box.style.display = "none";
            let per;
            let radbut = document.getElementsByName("vibor");
            for (var i = 0; i < radbut.length; i++) {
                if (radbut[i].checked){
                    per = radbut[i].value;
                }
            }
            let stoit = polePrice().vibor[per];
            if(poleKol()){
                let itog = parseInt(kol)*(polePrice().Nikon[1] + stoit);
                price.innerHTML = itog + " рублей";
            }
            break;
        case "3":
            radk.style.display = "none";
            box.style.display = "block";
            let sumop = 0;
            let d = document.getElementsByName("dop");
            for (let i = 0; i < d.length ; i++) {
                if(d[i].checked){
                    sumop += polePrice().doppole[d[i].value];
                }
            }
            if(poleKol()){
                let itog = parseInt(kol) * (polePrice().Nikon[2] + sumop);
                price.innerHTML = itog + " рублей";
            }
            break;
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    let radk = document.getElementById("radioknop");
    radk.style.display = "none";
    let box = document.getElementById("checkboxepole");
    box.style.display = "none";

    let kol = document.getElementById("kol");
    kol.addEventListener("change", function (event) {
        console.log("Kol was changed");
        price();
    });

    let select = document.getElementById("Nikon");
    select.addEventListener("change", function (event) {
        console.log("Model was changed");
        price();
    });

    let radioknop = document.getElementsByName("vibor");
    radioknop.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            console.log("Lens was changed");
            price();
        });
    });
    let checkboxepole = document.querySelectorAll("#checkboxepole input");
    checkboxepole.forEach(function (checkbox) {
        checkbox.addEventListener("change", function (event) {
            console.log("Options was changed");
            price();
        });
    });
    price();
});
